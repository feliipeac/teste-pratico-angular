import {Component, OnInit} from '@angular/core';
import {PostServiceService} from '../post-service.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  post: any;

  constructor(private route: ActivatedRoute,private serviceDetails: PostServiceService) {
    this.route.params.subscribe( params => {
      this.serviceDetails.getPostId(params.id).subscribe(itens => {
        this.post = itens;
      });
    } );
  }

  ngOnInit() {

  }

}
