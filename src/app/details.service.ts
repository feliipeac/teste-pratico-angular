import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class DetailsService {

  private url_users = 'https://jsonplaceholder.typicode.com/users/';
  constructor(private http: HttpClient) {}

  getUserId(userId) {
    return this.http.get(this.url_users + userId);
  }

}
