import { Component } from '@angular/core';
import {PostServiceService} from './post-service.service';
import {UsersService} from './users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  posts: any;
  users: any;

  title = 'app';

  constructor(private service: PostServiceService, private serviceU: UsersService) {


  }
}
