import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class PostServiceService {
  private url_posts = 'https://jsonplaceholder.typicode.com/posts/';
  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get(this.url_posts);
  }
  getPostId(postId) {
    return this.http.get(this.url_posts+postId)
  }
}
