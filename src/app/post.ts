export class Post {
  public idUser: number;
  public idPost: number;
  public titlePost: string;
  public textPost: string;
}
