
import { DetailsComponent } from './details/details.component';
import {HomeComponent} from './home/home.component';
import {UsersComponent} from './users/users.component';
import {PostComponent} from './post/post.component';

export const routes = [
  {
    path: 'posts/:id',
    component: PostComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'user/:id',
    component: DetailsComponent
  }

];
