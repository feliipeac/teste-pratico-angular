import { Component, OnInit } from '@angular/core';
import {UsersService} from '../users.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any;
  constructor(private serviceU: UsersService) {
  }

  ngOnInit() {
    this.serviceU.getUsers().subscribe(itemU => this.users = itemU);
  }


}
