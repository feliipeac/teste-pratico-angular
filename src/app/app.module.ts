import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PostComponent } from './post/post.component';
import {PostServiceService} from './post-service.service';
import {HttpClientModule} from '@angular/common/http';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users.service';
import { DetailsComponent } from './details/details.component';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import {routes} from './app.router';


@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    UsersComponent,
    DetailsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [PostServiceService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
