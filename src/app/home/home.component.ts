import {Component, OnInit} from '@angular/core';
import {PostServiceService} from '../post-service.service';
import {UsersService} from '../users.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  posts: any;

  constructor(private service: PostServiceService, private serviceU: UsersService) {
    this.service.getPosts().subscribe((itens: Array<any>) => {
      this.posts = itens;

      for (let i = 0; i < itens.length; i++) {
        this.serviceU.getUserId(this.posts[i].userId).subscribe(usuario => this.posts[i].userName = usuario.name);
      }
    });

  }

  ngOnInit() {
  }

}
