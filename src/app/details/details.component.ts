import {Component, OnInit} from '@angular/core';
import {PostServiceService} from '../post-service.service';
import {UsersService} from '../users.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  user: any;
  constructor(private route: ActivatedRoute, private serviceDetails: UsersService) {
    this.route.params.subscribe( params => {
      this.serviceDetails.getUserId(params.id).subscribe(itens => {
        this.user = itens;
      });
    } );
  }

  ngOnInit() {

  }

}
