import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UsersService {
  private url_users = 'https://jsonplaceholder.typicode.com/users/';
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(this.url_users);
  }
  getUserId(userId) {
    return this.http.get(this.url_users + userId);
  }

}
